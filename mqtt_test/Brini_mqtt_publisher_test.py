"""
    CLASSE 5^CTL - TPS
    SCOPO: Realizzare un progetto con l'uso del protocollo MQTT

    BRINI LUCA - MQTT.js TEST
    Questo File contiene un test per la libreria paho-mqtt - publisher

"""


import paho.mqtt.client as mqtt
import random as random
import time
import json

connect_flag = False

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    global connect_flag
    connect_flag = True

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("mosquitto.lucabrini.it", 1883, 60)

client.loop_start()

while True:
    
    if(connect_flag):
        payload = json.dumps({
            "temperature" : random.randint(0, 30),
            "relative_humidity" : random.randint(0, 100)
        })
        print(payload)
        client.publish("/env/htu21", payload=payload, qos=2 )

        time.sleep(30)

client.loop_stop()