"""
    CLASSE 5^CTL - TPS
    SCOPO: Realizzare un progetto con l'uso del protocollo MQTT

    BRINI LUCA - MQTT.js TEST
    Questo File contiene un test per la libreria paho-mqtt - subscriber

"""

import paho.mqtt.client as mqtt
import random as random
import time
import json

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("/env/htu21")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(json.loads((msg.payload).decode('utf-8')))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("mosquitto.lucabrini.it", 1883, 60)

client.loop_forever()