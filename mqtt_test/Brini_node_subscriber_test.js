/*
    CLASSE 5^CTL - TPS
    SCOPO: Realizzare un progetto con l'uso del protocollo MQTT

    BRINI LUCA - MQTT.js TEST
    Questo File contiene un test per la libreria mqtt.js - subscriber

*/

var mqtt = require('mqtt')
var options = {
    protocol: 'ws',
    port: 1884,
    // clientId uniquely identifies client
    // choose any string you wish
    clientId: 'b0908853'
};
var client  = mqtt.connect('ws://87.0.91.135', options);
 
client.on('connect', function () {
  client.subscribe('/env/htu21', function (err) {
  })
})
 
client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())
  client.end()
})