/*
    CLASSE 5^CTL - TPS
    SCOPO: Realizzare un progetto con l'uso del protocollo MQTT

    BRINI LUCA - WEBSite MQTT Dashboard
    Questo File contiene l'applicazione web sviluppata con react native (Parte Subscriber)
    Per una demo visitare https://progettomqtt.lucabrini.it/

*/

import { Component } from "react";
import { Button, Dropdown, DropdownItem } from '@windmill/react-ui'
import { Line } from '@reactchartjs/react-chart.js'
var mqtt = require('mqtt');

var options = {
    protocol: 'ws', // per usare il protocollo mqtt in una webapp bisogna utilizzare mqtt over websocket
    port: 1884, //porta server mosquitto che gira sul raspberry
    clientId: 'b0908853',
};
var client = mqtt.connect('ws://87.0.91.135', options); //creo client mqtt
client.setMaxListeners(1)

class App extends Component {
    //costruttore, inizializza lo state
    constructor(props) {
        super(props)

        this.state = {
            bmp180_temp: [],
            bmp180_press: [],
            bmp180_alt: [],
            htu21_temp: [],
            htu21_hum: [],
            dataset: []
        }
    }
    // funzione che viene richiamata appena il componente viene renderizzato sulla pagina html
    componentDidMount() {
        this.setState({ //setta la variabile dataset nello stato in modo che in automatico ri-renderizzi la pagina
            dataset: [
                {
                    label: 'Temperatura BMP',
                    data: this.state.bmp180_temp,
                    fill: false,
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgba(255, 99, 132, 0.2)',
                },
                {
                    label: 'Temperatura HTU',
                    data: this.state.htu21_temp,
                    fill: false,
                    backgroundColor: 'rgb(0, 99, 132)',
                    borderColor: 'rgba(0, 99, 132, 0.2)',
                },
            ],
        })
    }
    //metodo per settare correttamente i dati, ricevuti da mqtt, nello state
    setData(topic, message, packet) { 
        console.log(packet)
        var mqtt_data = JSON.parse(message.toString()) //parso dati che arrivano da broker
         switch (topic) {
            case "/env/bmp180":
                var alt = this.state.bmp180_alt, press = this.state.bmp180_press, temp = this.state.bmp180_temp;
                alt.push(mqtt_data.altitude) //pusho il nuovo valore nell'array di dati preesistente
                press.push(mqtt_data.pressure)
                temp.push(mqtt_data.temperature)
                this.setState({ bmp180_alt: alt, bmp180_press: press, bmp180_temp: temp })
                break
            case "/env/htu21":
                var temp = this.state.htu21_temp, hum = this.state.htu21_hum
                temp.push(mqtt_data.temperature)
                hum.push(mqtt_data.relative_humidity)
                this.setState({ htu21_hum: hum, htu21_temp: temp })
                break
        } 
    }

    shouldComponentUpdate(){
        return false
    }

    //metodo render per mostrare gli elementi sull pagina web
    render() {
        client.on('connect', function () { //metodo on della classe mqtt, nella libreria
            console.log('connected')
            client.subscribe('/env/bmp180', function (err) { //quando si connette al server, si sottoscrive ai due topic
                console.log('subcribed to /bmp180/temp')
            })
            client.subscribe('/env/htu21', function (err) {
                console.log('subcribed to /htu21/temp')
            })
        })

        client.on('message', (topic, message, packet) => { //quando arriva un messaggio, chiama la funzione per inserire i dati nello stato
            console.log("client.on: " + message.toString())
            this.setData(topic, message, packet)
        })

        return (
            <div>
                <nav className="flex items-center justify-between px-6 py-2 rounded-lg bg-gray-50 dark:bg-gray-800 shadow-lg">
                    <a className="text-gray-700 dark:text-gray-400">
                        MQTT Dashboard
                    </a>
                    <ul className="flex space-x-4">
                        <li>
                            <Button layout="link">Francesco Gritti</Button>
                        </li>
                        <li>
                            <Button layout="link">Luca Brini</Button>
                        </li>
                        <li>
                        </li>
                    </ul>
                </nav>

                <div className="mx-8 my-8 bg-white shadow overflow-hidden sm:rounded-lg">
                    <div className="px-4 py-5 sm:px-6">
                        <h3 className="px-4 py-8 text-lg leading-6 font-medium text-gray-900">
                            <MyChart dataset={this.state.dataset}></MyChart>
                        </h3>
                        <div>
                            <Button onClick={() => this.setState({ openDrop: !this.state.openDrop })}>Seleziona grandezza</Button>
                            <Dropdown isOpen={this.state.openDrop} onClose={() => this.setState({ openDrop: false })}>
                                <DropdownItem tag="a" onClick={() => this.setState({ //setto il dataset corretto per selezione
                                    dataset: [
                                        {
                                            label: 'Temperatura BMP',
                                            data: this.state.bmp180_temp,
                                            fill: false,
                                            backgroundColor: 'rgb(255, 99, 132)',
                                            borderColor: 'rgba(255, 99, 132, 0.2)',
                                        },
                                        {
                                            label: 'Temperatura HTU',
                                            data: this.state.htu21_temp,
                                            fill: false,
                                            backgroundColor: 'rgb(0, 99, 132)',
                                            borderColor: 'rgba(0, 99, 132, 0.2)',
                                        },
                                    ],
                                    openDrop: false
                                })} className="justify-between">
                                    <span>Temperatura</span>
                                </DropdownItem>
                                <DropdownItem tag="a" onClick={() => this.setState({
                                    dataset: [
                                        {
                                            label: 'Pressione',
                                            data: this.state.bmp180_press,
                                            fill: false,
                                            backgroundColor: 'rgb(255, 99, 132)',
                                            borderColor: 'rgba(255, 99, 132, 0.2)',
                                        },
                                    ],
                                    openDrop: false
                                })} className="justify-between">
                                    <span>Pressione</span>
                                </DropdownItem>
                                <DropdownItem tag="a" onClick={() => this.setState({
                                    dataset: [
                                        {
                                            label: 'Umidità',
                                            data: this.state.htu21_hum,
                                            fill: false,
                                            backgroundColor: 'rgb(255, 99, 132)',
                                            borderColor: 'rgba(255, 99, 132, 0.2)',
                                        },
                                    ],
                                    openDrop: false
                                })} className="justify-between">
                                    <span>Umidità</span>
                                </DropdownItem>
                            </Dropdown>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

/* Componente per il grafico */
class MyChart extends Component{

    constructor(props){
        super(props)
    }

    render(){
        return(
        <Line data={{
            datasets: this.props.dataset //dataset di dati, viene aggiornato a seconda del dropdownbutton
        }} options={{
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true,
                        },
                    },
                ],
            },
        }} height={100}
        />)
    }
}

export default App